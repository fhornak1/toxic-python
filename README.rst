Toxic-Python
============

Docker image usable within CI for testing python on multiple versions.
It's docker image with installed multiple versions of python and tox.

**Python versions:**
 - 2.7.3 (wheezy)
 - 2.7.9 (jessie)
 - 2.7.13 (stretch)
 - 3.4.2 (jessie)
 - 3.5.6 (stretch)
 - 3.6.8 (bionic-beaver)
 - 3.7.3 (stable python)

Tox is runned from python `3.7.3`

Build
-----

Building docker image is fairly straitforward, just call `./build.sh`
