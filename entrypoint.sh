#!/bin/bash -e

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

if [ "$1" = "tox" ]; then
	pyenv local ${PYTHON_VERSIONS}
	tox
else
	eval "$@"
fi
