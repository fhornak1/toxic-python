#!/bin/bash -e

DATE="$(date +%Y%V)"

PYTHON_VERSIONS="2.7.3 2.7.9 2.7.13 3.4.2 3.5.6 3.6.8 3.7.3"
PYTHON_GLOBAL="3.7.3"
POSITIONAL=()

print_help_end_exit() {
	cat << EOF
Builds python-toxic docker image.
usage:
	./build.sh [--python-versions "2.7.9 2.7.13 3.5.6 3.7.3"] \
		[-g|--python-global 3.7.3] \
		[-h|--help] \
		<IMAGE_NAME>

Positional arguments
	IMAGE_NAME		Name of built image.

Optional arguments:
	-h|--help		Print this help and exit.
	--python-versions	Versions of python interpreter that
				should be installed.
				[default: "2.7.3 2.7.9 2.7.13 3.4.2 3.5.6 3.7.3"]
	-g|--python-global	Specifies default python.
				[default: "3.7.3"]
EOF
	exit 1
}

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --python-versions)
    PYTHON_VERSIONS="$2"
    shift
    shift
    ;;
    -g|--python-global)
    PYTHON_GLOBAL="$2"
    shift
    shift
    ;;
    -h|--help)
    print_help_end_exit
    shift
    ;;
    *)
    POSITIONAL+=("$1")
    shift
    ;;
esac
done
set -- "${POSITIONAL[@]}"

docker build -t "$1" -f Dockerfile \
	--build-arg DATE="$DATE" \
	--build-arg VERSION="1.0.0" \
	--build-arg PYTHON_VERSIONS="$PYTHON_VERSIONS" \
	--build-arg PYTHON_GLOBAL="$PYTHON_GLOBAL" \
	.
