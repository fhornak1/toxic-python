FROM debian:stretch-slim

ARG VERSION
ARG DATE

ENV PYENV_ROOT=/opt/pyenv \
    PATH=/opt/pyenv/bin:/opt/pyenv/shims/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN set -e; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	        ca-certificates \
		make \
		build-essential \
		libssl-dev \
		zlib1g-dev \
		libbz2-dev \
		libreadline-dev \
		libsqlite3-dev \
		wget \
		curl \
		llvm \
		libncurses5-dev \
		libncursesw5-dev \
		xz-utils \
		tk-dev \
		libffi-dev \
		liblzma-dev \
		git; \
	curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash; \
	apt-get autoremove -y; \
	apt-get clean all; \
	rm -rf /var/lib/apt/lists/*;

ARG PYTHON_VERSIONS="2.7.3 2.7.9 2.7.13 3.4.2 3.5.6 3.6.8 3.7.3"
ARG PYTHON_GLOBAL="3.7.3"

ENV PYTHON_VERSIONS=$PYTHON_VERSIONS \
    PYTHON_GLOBAL=$PYTHON_GLOBAL

RUN set -e; \
	apt-get update; \
	apt-get install -y libssl1.0-dev; \
	rm -rf /var/lib/apt/lists/*; \
	for version in $PYTHON_VERSIONS; do \
		pyenv install $version; \
	done;

RUN set -e; \
	pyenv global "$PYTHON_GLOBAL"; \
	pip install -U pip setuptools virtualenv; \
	pip install tox tox-pyenv;

COPY entrypoint.sh /usr/bin

ENTRYPOINT ["entrypoint.sh"]

VOLUME ["/tmp/app"]

WORKDIR "/tmp/app"

CMD ["tox"]
